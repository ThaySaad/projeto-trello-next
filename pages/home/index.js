import React from "react";
import "../home/home.module.css";

export default function Home() {
    const teste = 2;

    /**
     * Function validation data
     * 
     * @param {object} item [{name: strind, age: number}]
     * @param {number} index id user
     * @returns {boolean} if test !==0 return true 
     */
    const funTest = (item, index) => {
        return teste;
    };

    return (
        <>
            <h1>Hello World!</h1>
        </>
    );
}
